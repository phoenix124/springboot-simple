package com.xiaoshu;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.web.filter.CharacterEncodingFilter;

/**
 * @author ubt
 */
@MapperScan("com.xiaoshu.mapper*")
@SpringBootApplication
@EnableAspectJAutoProxy
public class AppSpringbootStartMain  extends SpringBootServletInitializer
{
	private static Logger logger = LoggerFactory.getLogger(AppSpringbootStartMain.class);

	public static void main(String[] args) {
		
		logger.info("start up the xiaoshu example server ...");
		SpringApplication.run(AppSpringbootStartMain.class);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder  application) {
		return application.sources(AppSpringbootStartMain.class);
	}
	/**
	 * 字符拦截，使用UTF-8编码
	 * @return
	 */
	@Bean
	public FilterRegistrationBean filterRegistrationBean() {
		FilterRegistrationBean registrationBean = new FilterRegistrationBean();
		CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
		characterEncodingFilter.setEncoding("UTF-8");
		registrationBean.setFilter(characterEncodingFilter);
		registrationBean.addUrlPatterns("/*");
		return registrationBean;
	}
	
}
