package com.xiaoshu.config.swagger;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 
 * @author ubt
 *
 */
@Component
@ConfigurationProperties(prefix="swagger")
//@PropertySource("classpath:swagger.properties")
public class SwaggerProperties {

	private String basePackage;
	
	private String contactName="Ubtech";
	
	private String contactUrl;
	
	private String contactEmail="ubtech.develpor@robot.com";

	
	
	public String getContactUrl() {
		return contactUrl;
	}

	public void setContactUrl(String contactUrl) {
		this.contactUrl = contactUrl;
	}

	public String getBasePackage() {
		return basePackage;
	}

	public void setBasePackage(String basePackage) {
		this.basePackage = basePackage;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
	
	
}
