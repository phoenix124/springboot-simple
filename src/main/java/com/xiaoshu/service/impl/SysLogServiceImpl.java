package com.xiaoshu.service.impl;

import com.xiaoshu.entity.SysLog;
import com.xiaoshu.mapper.SysLogMapper;
import com.xiaoshu.service.SysLogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 操作日志表 服务实现类
 * </p>
 *
 * @author deane.jia
 * @since 2017-11-02
 */
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLog> implements SysLogService {
	
}