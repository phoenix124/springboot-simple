package com.xiaoshu.service;

import com.xiaoshu.entity.SysLog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 操作日志表 服务类
 * </p>
 *
 * @author deane.jia
 * @since 2017-11-02
 */
public interface SysLogService extends IService<SysLog> {
	
}
