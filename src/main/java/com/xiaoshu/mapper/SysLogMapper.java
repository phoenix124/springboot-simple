package com.xiaoshu.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.xiaoshu.entity.SysLog;

/**
 * <p>
  * 操作日志表 Mapper 接口
 * </p>
 *
 * @author deane.jia
 * @since 2017-11-02
 */
public interface SysLogMapper extends BaseMapper<SysLog> {

}