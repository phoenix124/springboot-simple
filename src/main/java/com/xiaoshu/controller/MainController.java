package com.xiaoshu.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.sql.DataSource;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.plugins.Page;
import com.xiaoshu.entity.ParamPage;
import com.xiaoshu.entity.SysLog;
import com.xiaoshu.service.SysLogService;

/**
 * 
 * @author ubt
 *
 */
@RestController
@RequestMapping(value = "/main")
@Api(tags = "Main page server")
public class MainController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private DataSource dataSource;

	private final ExecutorService exec = Executors.newFixedThreadPool(1);
	
	private final long WAIT_MAX_TIME_SECOND = 15;
	
	private final static String DefaultValidationQuery = "SELECT 'X'";
	
	private final static String SIZE_UNIT = "M";

	private final static long size = 1024 * 1024;
	
	@Autowired
	private SysLogService logService;

	@PostMapping(value="/page",produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiOperation(value="分页进行查询日志结果")
	public Page<SysLog> selectByPage(@ApiParam @RequestBody @Valid ParamPage params,Errors errors) {
		if(errors.hasErrors()){
			System.out.println(errors.getAllErrors().toString());
		}
		logger.info("start query th page...");
		Page<SysLog> pageParams = new Page<SysLog>(params.getPageIndex(), params.getPageSize());
		Page<SysLog> pages = logService.selectPage(pageParams);
		return pages;
	}
	
	@GetMapping(value = "/status", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiOperation(value="服务器系统状态，CPU、Memory等状态")
	public String monitorServerStatus() {
		logger.info("get The Server Status info ...");
		Properties p = System.getProperties();// 获取当前的系统属性
		// p.list(System.out);// 将属性列表输出

		final Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("OperateSystem", p.getProperty("sun.desktop"));
		resultMap.put("CpuNum", Runtime.getRuntime().availableProcessors());
		// 虚拟机内存总量
		resultMap.put("TotalMemory", Runtime.getRuntime().totalMemory() / size + SIZE_UNIT);
		// 虚拟机空闲内存量
		resultMap.put("FreeMemory", Runtime.getRuntime().freeMemory() / size + SIZE_UNIT);
		// 虚拟机使用最大内存量
		resultMap.put("MaxMemory", Runtime.getRuntime().maxMemory() / size + SIZE_UNIT);
		// 提交线程任务，并在指定的时间内返回，否则视为失败
		Callable<String> call = new Callable<String>() {
			@Override
			public String call() throws Exception {
				// 开始执行耗时操作
				final Connection conn = dataSource.getConnection();
				try {
					if (conn != null && !conn.isClosed()) {
						Statement stmt = conn.createStatement();
						try {
							boolean flag = stmt.execute(DefaultValidationQuery);
							if (flag) {
								resultMap.put("DataBaseConn", " success ");
								resultMap.put("status", 1);
							} else {
								resultMap.put("DataBaseConn", "Query Exception");
								resultMap.put("status", 0);
							}
						} catch (Exception e) {
							resultMap.put("status", -1);
							resultMap.put("DataBaseConn", "Connection Error");
							e.printStackTrace();
						} finally{
							if(stmt != null && !stmt.isClosed()){
								stmt.close();
							}
							if(conn != null && !conn.isClosed()){
								conn.close();
							}
						}
					} else {
						resultMap.put("DataBaseConn", "Connection closed");
						resultMap.put("status", 0);
					}
				} catch (SQLException e) {
					resultMap.put("status", -1);
					resultMap.put("DataBaseConn", "Connection Error");
					e.printStackTrace();
				} 
				return "execute Thread finished.";
			}
		};
		// 通过执行时间判断是否快速返回结果信息
		try {
			Future<String> future = exec.submit(call);
			String obj = future.get(WAIT_MAX_TIME_SECOND, TimeUnit.SECONDS); // 任务处理超时时间设为 10 秒
			logger.info("Execute success:{}", obj);
		} catch (Exception e) {
			resultMap.put("status", -1);
			resultMap.put("DataBaseConn", "Connection Error");
			e.printStackTrace();
		}

		return JSON.toJSONString(resultMap);
	}
}
