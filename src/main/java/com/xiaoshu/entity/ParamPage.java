package com.xiaoshu.entity;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class ParamPage implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8693410436410119325L;

	@ApiModelProperty("分页大小")
	@NotNull
	private int pageSize;

	@ApiModelProperty("分页号")
	@NotNull
	private int pageIndex;

	@ApiModelProperty("内容")
	@NotNull
	private String content;

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
